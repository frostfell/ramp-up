//
//  Ramp.swift
//  ramp-up
//
//  Created by Igor Chernyshov on 31.03.2018.
//  Copyright © 2018 Igor Chernyshov. All rights reserved.
//

import Foundation
import SceneKit

class Ramp {
    
    class func getRampForName(rampName: String) -> SCNNode {
        switch rampName {
        case "pipe":
            return Ramp.getPipe()
        case "quarter":
            return Ramp.getQuarter()
        case "pyramid":
            return Ramp.getPyramid()
        default:
            return Ramp.getPipe()
        }
    }
    
    class func getPipe() -> SCNNode {
        let obj = SCNScene(named: "art.scnassets/pipe.dae") // We look inside the scene
        let node = obj?.rootNode.childNode(withName: "pipe", recursively: true) // And grab an object named pipe from there
        node?.scale = SCNVector3Make(0.0022, 0.0022, 0.0022) // Set scale to this size (made up experimentally)
        node?.position = SCNVector3Make(-1, 0.7, -1) // Set the position (made up expirementally)
        node?.eulerAngles = SCNVector3Make(0, .pi/3, 0) // Set the initial angle. This puts ramps into different orientation at the beginnig and looks better
        return node!
    }
    
    class func getPyramid() -> SCNNode {
        let obj = SCNScene(named: "art.scnassets/pyramid.dae")
        let node = obj?.rootNode.childNode(withName: "pyramid", recursively: true)
        node?.scale = SCNVector3Make(0.0058, 0.0058, 0.0058)
        node?.position = SCNVector3Make(-1, -0.45, -1)
        node?.eulerAngles = SCNVector3Make(0, .pi/4, 0)
        return node!
    }
    
    class func getQuarter() -> SCNNode {
        let obj = SCNScene(named: "art.scnassets/quarter.dae")
        let node = obj?.rootNode.childNode(withName: "quarter", recursively: true)
        node?.scale = SCNVector3Make(0.0058, 0.0058, 0.0058)
        node?.position = SCNVector3Make(-1, -2.2, -1)
        node?.eulerAngles = SCNVector3Make(0, .pi, 0)
        return node!
    }
    
    class func startRotation(node: SCNNode) {
        let rotate = SCNAction.repeatForever(SCNAction.rotateBy(x: 0, y: CGFloat(0.01 * Double.pi), z: 0, duration: 0.1)) // Create an action that does the following: every 0.1 of a second rotate the figure bi that much and repeat it forever
        node.runAction(rotate)
    }
}
