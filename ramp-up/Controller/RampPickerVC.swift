//
//  RampPickerVC.swift
//  ramp-up
//
//  Created by Igor Chernyshov on 27.03.2018.
//  Copyright © 2018 Igor Chernyshov. All rights reserved.
//

import UIKit
import SceneKit

class RampPickerVC: UIViewController {

    var sceneView: SCNView!
    var size: CGSize!
    weak var rampPlacerVC: RampPlacerVC! // Weak var to avoid memory leak because we won't have a process to decomission that VC after use
    // If there are 2 objects referencing each other, one of them has to be weak or you get a memory leak (:
    
    init(size: CGSize) {    // Will be called whenever we initialize this VC
        super.init(nibName: nil, bundle: nil)
        self.size = size
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.frame = CGRect(origin: CGPoint.zero, size: size) // This is required because new VC is not a fullscreen thing
        sceneView = SCNView(frame: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        view.insertSubview(sceneView, at: 0)
        
        preferredContentSize = size // Very important to not to forget this or VC goes full screen
        
        let scene = SCNScene(named: "art.scnassets/ramps.scn")! // Load "Background" for ramps picker window
        sceneView.scene = scene
        
        let camera = SCNCamera()    // The following lines set an orthoraphic camera
        camera.usesOrthographicProjection = true    // Which means it removes depth from the view
        scene.rootNode.camera = camera  // So it's basically 2D but looks like 3D
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        sceneView.addGestureRecognizer(tap)
        
        let pipe = Ramp.getPipe()
        Ramp.startRotation(node: pipe)
        scene.rootNode.addChildNode(pipe)
        
        let pyramid = Ramp.getPyramid()
        Ramp.startRotation(node: pyramid)
        scene.rootNode.addChildNode(pyramid)
        
        let quarter = Ramp.getQuarter()
        Ramp.startRotation(node: quarter)
        scene.rootNode.addChildNode(quarter)
    }
    
    @objc func handleTap(_ gestureRecognizer: UIGestureRecognizer) {
        let p = gestureRecognizer.location(in: sceneView) // Find the location of the tap and store it in P
        let hitResult = sceneView.hitTest(p, options: [:]) // P = point, options = [ EMPTY : DICTIONARY]. Test if any object was hit by tap
        if hitResult.count > 0 {    // If something was hit by tap it returns more than 0
            let node = hitResult[0].node // Grab that node (it's in 0 index)
            rampPlacerVC.onRampSelected(node.name!)
        }
    }
}
