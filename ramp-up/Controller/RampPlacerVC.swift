//
//  RampPlacerVC.swift
//  ramp-up
//
//  Created by Igor Chernyshov on 27.03.2018.
//  Copyright © 2018 Igor Chernyshov. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class RampPlacerVC: UIViewController, ARSCNViewDelegate, UIPopoverPresentationControllerDelegate { // The last one is required to display another VC as a popover

    @IBOutlet var sceneView: ARSCNView!
    var selectedRamp: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        // Show statistics such as fps and timing information
        sceneView.showsStatistics = true
        
        // Create a new scene
        let scene = SCNScene(named: "art.scnassets/main.scn")! // Load empty scene by default
        sceneView.autoenablesDefaultLighting = true // Enable default lighting
        
        // Set the scene to the view
        sceneView.scene = scene
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()

        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    // MARK: - ARSCNViewDelegate
    
/*
    // Override to create and configure nodes for anchors added to the view's session.
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        let node = SCNNode()
     
        return node
    }
*/
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {  // All new VC's will go full screen, no matter what, if this function is missing
        return .none
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) { // Call it every time it detects a tap
        guard let touch = touches.first else { return } // Just in case there was no touch actually...
        let results = sceneView.hitTest(touch.location(in: sceneView), types: [.featurePoint]) // .featurePoimts detects walls and floors and associates hit results with that surfaces
        guard let hitFeature = results.last else { return } // You may get nothing if the room is to dark or it only see a floor without context and so on
        let hitTransform = SCNMatrix4(hitFeature.worldTransform) // Calculate the vector from our position to a tap position
        let hitPosition = SCNVector3Make(hitTransform.m41,  hitTransform.m42, hitTransform.m43) // Create a new vector out of transform that can be used to placed an object at destination point
        placeRamp(position: hitPosition)
    }
    
    @IBAction func onRampButtonPressed(_ sender: UIButton) { // Changed any to UIButton here because we know that sender is always a Button here
        let rampPickerVC = RampPickerVC(size: CGSize(width: 250, height: 500))  // The popover VC will be that size (just guessing the size)
        rampPickerVC.rampPlacerVC = self
        rampPickerVC.modalPresentationStyle = .popover  // Set presentation style to popover at last
        rampPickerVC.popoverPresentationController?.delegate = self // Set delegate as it is declared in the beggining
        present(rampPickerVC, animated: true, completion: nil)  // Present the VC with all that parameters
        rampPickerVC.popoverPresentationController?.sourceView = sender // These two lines will bind the VC to bounds of the Button itself
        rampPickerVC.popoverPresentationController?.sourceRect = sender.bounds
    }
    
    func onRampSelected(_ rampName: String) {
        selectedRamp = rampName
    }
    
    func placeRamp(position: SCNVector3) {
        if let rampName = selectedRamp { // If selectedRamp exists (because user can crash it)
            let ramp = Ramp.getRampForName(rampName: rampName)  // Return a ramp from the Ramp Class
            ramp.position = position    // Send it position to the one we found before
            ramp.scale = SCNVector3Make(0.01, 0.01, 0.01) // Make it a default size instead of mini that we had in preview
            sceneView.scene.rootNode.addChildNode(ramp) // Add it to the main scene
        }
    }
}
